package main

import (
	"github.com/karachain/mining/ethash"
	"math/big"
	"fmt"
)

func main()  {
	var maxUint256 = new(big.Int).Exp(big.NewInt(2), big.NewInt(256), big.NewInt(0))
	var eth ethash.Ethash
	dataset := eth.Getdataset(1024)

	hash := "0x73550c3de574b12bb67d3ac32d544c9d7281e5d8cf94cd35840df0df787db15a"
	var nonce uint64
	nonce = 0
	target := new(big.Int).Div(maxUint256, big.NewInt(45000))
	for {
		fmt.Println("Nonce : ", nonce)
		digest, result := ethash.HashimotoCustom(dataset, []byte(hash), nonce)
		if new(big.Int).SetBytes(result).Cmp(target) <= 0 {
			fmt.Println("Found successfully nonce : ", nonce)
			fmt.Println("Digest : ", digest)
			return
		}
		nonce++
	}
}